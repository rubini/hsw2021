#ifndef __CPU_H__
#define __CPU_H__

#ifdef __ASSEMBLER__

#  define JIFFIES_ADDR		0x40018008

#else /* not assembler */

#include <stdint.h>

#  include <cpu-11xx.h>
#  include <gpio-11xx.h>

#define XTAL_FREQ  (12 * 1000 * 1000)
#define CPU_FREQ (XTAL_FREQ * CONFIG_PLL_CPU)

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */

