#include <io.h>
#include <debug-registers.h>

struct cpu_regs cpu_regs;

void registers_print(struct cpu_regs *r)
{
	int i;
	uint32_t *p = (void *)&cpu_regs;

	static const char *names[] = {
		"r0 ", "r1 ", "r2 ", "r3 ", "r4 ", "r5 ", "r6 ", "r7 ",
		"r8 ", "r9 ", "r10", "r11", "r12", "sp ", "lr ", "pc ",
		"msp", "psp", "psr", "pri", "con"
	};
	for (i = 0; i < ARRAY_SIZE(names); i++)
		printf("%s %08lx (%li)\n", names[i],
		       (long)p[i], (long)p[i]);
}

