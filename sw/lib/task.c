#include <io.h>
#include <irq.h>
#include <time.h>
#include <panic.h>
#include <timestamp-lpc.h>
#include <task.h>

extern struct task __task_first[], __task_last[];

static void sched_wcet_begin(struct task *t)
{
	if (CONFIG_CALC_WCET)
		__timestamp_get(&t->start_time);
}
static void sched_wcet_end(struct task *t)
{
	unsigned long c1, c2;
	struct timestamp ts2;

	if (CONFIG_CALC_WCET) {
		__timestamp_get(&ts2);
		c1 = timestamp_to_64(&t->start_time);
		c2 = timestamp_to_64(&ts2);
		if ((c2 - c1) > t->wcet)
			t->wcet = c2 - c1;
	}
}

static void sched_wcet_print(void)
{
	struct task *t;

	if (!CONFIG_WCET_INTERVAL)
		return;

	for (t = __task_first; t < __task_last; t++)
		printf("task %-12s: %5li cycles\n", t->name, t->wcet);
}

 /*
 * This function is a simple helper, run in user space
 */
static void task_run(void *arg)
{
	struct task *t = arg;
	t->job(t->arg, t);
}

/*
 * This is the interrupt handler that serves the interrupt threads.
 * It does preemption by calling __sched() (which returns to "user space".
 */
static volatile int need_resched;

struct task *current;

static irqreturn_t schedule_irq(void *data, uint32_t ignored)
{
	struct task *t = data;
	struct task *prev = current;

	/* First: disable the iirq, whether we run or postpone it */
	regs[REG_NVIC_ICER0] = lpc_irq_bit(t->irq);

	if (!current || (t->prio > current->prio)) {
		/* preempt: run it immediately */
		current = t;
		sched_wcet_begin(t);
		__sched(task_run, t);
		sched_wcet_end(t);
		current = prev;

		/* enable back: we are done and ready for next irq */
		regs[REG_NVIC_ISER0] = lpc_irq_bit(t->irq);
		return IRQ_HANDLED;
	}

	/* No preemption: mark the task as active for later execution */
	t->flags |= __TASK_FLAG_ACTIVE;
	need_resched = 1;
	return IRQ_WAKE_THREAD;
}


/*
 * Init loop, calls init, requests interrupts and returns number of errors
 */
static int schedule_init(int v)
{
	struct task *t;
	int i, errors = 0;

	for (t = __task_first; t < __task_last; t++) {
		if (v)
			printf("Task: \"%s\": ", t->name);
		i = 0;
		if (t->init)
			i = t->init(t->arg, t);
		if (v)
			printf(i == 0 ? "Ok\n" : "KO\n");
		if (i != 0)
			errors++;
		/* fix prio and flags after t->init could touch them */
		if (!t->prio) {
			t->prio = t->flags & TASK_FLAG_IRQ
				? TASK_PRIO_IRQ_DEFAULT
				: TASK_PRIO_USER_DEFAULT;
		}
		if (t->flags & TASK_FLAG_IRQ) {
			i = irq_request(t->irq, 0, schedule_irq, t);
			if (i != 0)
				errors++;
		} else if (t->period == 0) {
			t->period = 1;
		}
	}
	return errors;
}

static unsigned long wcet_print_timeout;

/*
 * Select and run one task, and optional calculate/print wcet
 */
static void schedule_loop(int v)
{
	struct task *t, *best = NULL;

	/* Find if any interrupt thread is pending. If so run them */
	need_resched = 0;
	for (t = __task_first; t < __task_last; t++) {
		if (t->flags & __TASK_FLAG_ACTIVE) {
			t->flags &= ~__TASK_FLAG_ACTIVE;
			current = t;
			sched_wcet_begin(t);
			t->job(t->arg, t);
			sched_wcet_end(t);
			current = NULL;
			/* re-enable the interrupt */
			regs[REG_NVIC_ISER0] = lpc_irq_bit(t->irq);
		}
	}

	/* Find the best "normal" task to be run */
	for (t = __task_first; t < __task_last; t++) {
		if (t->flags & TASK_FLAG_IRQ)
			continue;
		if (!best)
			best = t;
		if  (time_before(t->release, best->release))
			best = t;
	}
	if (!best)
		return;

	/* Wait for it to be released, but if we got an activation, restart */
	while (time_before(jiffies, best->release))
		if (need_resched)
			return;
	if (v)
		printf("%6li: %10s: @ %6li\n", jiffies,
		       best->name, best->release);
	current = best;
	sched_wcet_begin(best);
	best->job(best->arg, best);
	sched_wcet_end(best);
	current = NULL;

	if (time_after(jiffies, wcet_print_timeout)) {
		sched_wcet_print();
		wcet_print_timeout += HZ * CONFIG_WCET_INTERVAL;
	}

	while (time_after_eq(jiffies, best->release))
		best->release += best->period;
}


void __attribute__((noreturn))  scheduler(int verbose)
{
	struct task *t;
	unsigned long j;
	int errors;

	/* Init them all */
	errors = schedule_init(verbose & SCHED_VERBOSE_INIT);

	/* Panic if any task failed init */
	if (errors)
		panic(3, "%i tasks failed at init time\n", errors);

	/* Fix release time, to ensure it is in the future */
	j = jiffies + 2 + HZ / 1000;
	for (t = __task_first; t < __task_last; t++)
		t->release += j;

	wcet_print_timeout = jiffies;

	irq_enable();

	/* And now run as expected, with verbose option */
	while (1)
		schedule_loop(verbose & SCHED_VERBOSE_CALL);
}
