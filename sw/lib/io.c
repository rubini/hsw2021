#include <io.h>
#include <errno.h>

int putc(int c)
{
	if (c == '\n')
		putc('\r');
	while ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		;
	regs[REG_U0THR] = c;

	return c;
}

int puts(const char *s)
{
	while (*s)
		putc (*s++);
	return 0;
}

/* getc is blocking */
int getc(void)
{
	while (!(regs[REG_U0LSR] & REG_U0LSR_RDR))
		;
	return regs[REG_U0RBR];
}

/* gets is blocking, because getc is */
char *gets(char *s)
{
	int c, i = 0;
	do {
		s[i++] = c = getc();
		s[i] = '\0';
	}
	while (c != '\n' && c != '\r');
	return s;
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int pollc(void)
{
	int ret = -EAGAIN;
	if (regs[REG_U0LSR] & REG_U0LSR_RDR)
		return regs[REG_U0RBR];
	return ret;
}

/* polls is non-blocking */
char *polls(char *s, int bsize)
{
	return polls_t(s, "\n\r", bsize);
}

char *polls_t(char *s, char *terminators, int bsize)
{
	int c, t, i = 0;

	if (bsize < 2)
		return NULL;
	c = pollc();
	if (c == -EAGAIN)
		return NULL; /* no news since last call */

	/* go to end of currently-accumulated string */
	while (s[i] && i < bsize - 1)
		i++;
	if (i == bsize - 1) /* caller made something wrong, start fresh */
		i = 0;
	s[i++] = c;
	s[i] = '\0';
	if (i == bsize - 1)
		return s;
	for (t = 0; terminators[t]; t++)
		if (c == terminators[t])
			return s;
	return NULL;
}
