#include <assert.h>
#include <irq.h>
#include <io.h>

struct irq_handler {
	irq_handler_t handler;
	void *data;
};

static struct irq_handler irq_handlers[__LPC_NR_IRQS];

static struct irq_handler *handlers;

/* The default handler */
static irqreturn_t no_irq(void *data, uint32_t timestamp_fraction)
{
	int irq = (int)data;
	irq_unexpected(irq, timestamp_fraction);
	return IRQ_NONE;
}

static int __irq_clear_ptr(int irq)
{
	struct irq_handler *h = irq_handlers + irq;

	h->handler = no_irq;
	h->data = (void *)irq;
	return 0;
}

/* We can't make this an initcall, or it will always be linked */
static void irq_init(void)
{
	int i;
	static int irq_initialized;

	if (irq_initialized)
		return;
	for (i = 0; i < ARRAY_SIZE(irq_handlers); i++)
		__irq_clear_ptr(i);
	irq_initialized++;
	handlers = irq_handlers; /* so irq_entry() finds them */
}

/*
 * This is overriding the assembly function that panics, but it still
 * calls it with the "irq_unexpected" name.
 */
void irq_entry(int irq, uint32_t timestamp_fraction)
{
	struct irq_handler *h;
	irqreturn_t retval;

	irq -= __NVIC_INTERNAL_INTERRUPTS;
	if (!handlers || irq < 0)
		irq_unexpected(irq, timestamp_fraction);

	h = handlers + irq;
	retval = h->handler(h->data, timestamp_fraction);

	/*
	 * And then acknowledge in the VIC. This works for level interrupts,
	 * not for edge ones, but there is no general solution.
	 */
	if (retval == IRQ_HANDLED)
		regs[REG_NVIC_ICPR0] = lpc_irq_bit(irq);

}


/* Called by an application, that will, later, irq_enable() */
int irq_request(int irq, unsigned long flags, irq_handler_t func, void *data)
{
	struct irq_handler *h = irq_handlers + irq;

	/* flags are undefined yet: panic if "newer" code relies on flags */
	assert(flags == 0, 127, "Invalid IRQ flags 0x%lx\n", flags);

	irq_init();

	if (irq < 0 || irq >= ARRAY_SIZE(irq_handlers))
		return -EINVAL;
	if (h->handler != no_irq)
		return -EBUSY;

	h->data = data;
	h->handler = func;
	regs[REG_NVIC_ISER0] = lpc_irq_bit(irq);

	return 0;
}

int irq_free(int irq, void *data)
{
	struct irq_handler *h = irq_handlers + irq;

	if (irq < 0 || irq >= ARRAY_SIZE(irq_handlers))
		return -EINVAL;
	if (h->data != data)
		return -ENOENT;

	regs[REG_NVIC_ICPR0] = lpc_irq_bit(irq); /* ack, to be sure */
	regs[REG_NVIC_ICER0] = lpc_irq_bit(irq); /* disable */
	return __irq_clear_ptr(irq);
}
