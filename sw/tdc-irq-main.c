#include <stdint.h>
#include <io.h>
#include <time.h>
#include <errno.h>
#include <irq.h>

/* Temporary values to help see things at the scope */
#define PIN_SQUARE GPIO_NR(1, 21)  /* MISO: pin 7 of uext */
#define PIN_IRQ    GPIO_NR(1, 22)  /* MOSI: pin 8 of uext */

#define irq_debug_enter() writel(1, __GPIO_WORD(PIN_IRQ))
#define irq_debug_leave() writel(0, __GPIO_WORD(PIN_IRQ))

static inline int loop_debug_toggle(int val)
{
	writel(val, __GPIO_WORD(PIN_SQUARE));
	return !val;
}

#define TDC_FORCE_DELAY 0 /* if you want, raise this to force overflows */

/* Mask of "interesting" bits for the TDC functionality */
static uint32_t tdc_mask = 0x00f1f000;
#define TDC_SHIFT		12

/*
 * This is the main structure. We sample within the timely
 * interrupts and save to a circular buffer of these.
 */
struct tdc_sample {
	uint32_t counter;
	uint32_t bitmask;
};

static struct tdc_sample tdc_samples[512];

static struct tdc_sample *tdc_wp, *tdc_last; /* write pointer and last */
static uint32_t tdc_nr; /* N read */
static volatile uint32_t tdc_nw; /* N written */
static uint32_t tdc_counter;

void __attribute__((aligned(16))) __irq_entry (void)
{
	uint32_t counter, bitmask;
	static uint32_t prev_bitmask = ~0;

	irq_debug_enter();
	counter = tdc_counter++; /* start with zero */
	bitmask = regs[REG_GPIO_MPIN0];
	if (bitmask != prev_bitmask) {
		prev_bitmask = bitmask;
		tdc_wp->counter = counter;
		tdc_wp->bitmask = bitmask;
		tdc_nw++;
		tdc_wp++;
		if (tdc_wp == tdc_last)
			tdc_wp = tdc_samples;
	}

	/* Finally, acknowledge and return */
	regs[REG_TMR16B0IR] = 1; /* MR0 */
	regs[REG_NVIC_ICPR0] = lpc_irq_bit(LPC_IRQ_CT16B0);
	irq_debug_leave();
}

static void tdc_print(uint32_t counter, int bitmask)
{
	printf("%08lx %03x\n", (long)counter, bitmask >> TDC_SHIFT);
}

static void tdc_loop(void)
{
	static struct tdc_sample *s = tdc_samples;
	static int debugpinval, lost = 0;
	uint32_t c1, c2;
	uint32_t bitmask;

	/* too fast? */
	while (time_before_eq(tdc_nw, tdc_nr)) {
		/* A signal for the scope, just in case */
		debugpinval = loop_debug_toggle(debugpinval);
	}
	/* "atomic" read of the item */
	c2 = s->counter;
	asm("" ::: "memory"); /* mb() */
	bitmask = s->bitmask;
	c1 = s->counter;
	if (c1 != c2) {
		asm("" ::: "memory"); /* mb() */
		bitmask = s->bitmask;
	}
	/* check for overflow */
	while (time_after_eq(tdc_nw, tdc_nr + ARRAY_SIZE(tdc_samples))) {
		tdc_nr += ARRAY_SIZE(tdc_samples);
		lost += ARRAY_SIZE(tdc_samples);
	}
	if (lost) {
		printf("lost: %x\n", lost + 1);
		lost = 0;
	} else {
		/* we don't know if it's new or old: discard */
		tdc_print(c1, bitmask);
	}


	/* more debug */
	if (TDC_FORCE_DELAY)
		udelay(TDC_FORCE_DELAY);

	/* done, prepare to do it again */
	tdc_nr++;
	s++;
	if (s == tdc_last) {
		s = tdc_samples;
	}
}


void main(void)
{
	int i;

	printf("TDC interrupt application (%s)\n", __GITC__);

	/* Debugging pins */
	gpio_dir_af(PIN_SQUARE, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(PIN_IRQ, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* Activate pull-down on all bits, to prevent noise */
	for (i = 0; i < 31; i++) {
		if (tdc_mask & (1 << i)) {
			gpio_dir_af(GPIO_NR(0, i), GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLDOWN);
		}
	}

	/* Setup mask reading (only some bits are returned) */
	regs[REG_GPIO_MASK0] = ~tdc_mask;

	/* Prepare circular buffer */
	tdc_wp = tdc_samples;
	tdc_last = tdc_samples + ARRAY_SIZE(tdc_samples);

	/* Configure a periodic timer */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT16B0; /* power on */
	regs[REG_TMR16B0TCR] = 2; /* reset counters, disable */
	regs[REG_TMR16B0PR] = (CPU_FREQ / 1000 / 1000 /2) - 1; /* 0.5us */
	regs[REG_TMR16B0MR0] = 7; /* match when counter = 7 (4us) */
	regs[REG_TMR16B0MCR] = 3; /* irq and reset counter */
	regs[REG_TMR16B0TCR] = 1; /* enable */

	/* And enable the associated interrupt */
	regs[REG_NVIC_ICER0] = lpc_irq_bit(LPC_IRQ_CT16B0);
	irq_enable();

	while (1) {

		tdc_loop();
	}
}
