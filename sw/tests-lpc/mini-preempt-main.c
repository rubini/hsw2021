#include <io.h>
#include <time.h>
#include <panic.h>
#include <irq.h>
#include <debug-registers.h>

/*
 * This is an example that runs two tasks, one preempting the other,
 * and the timer interrupt runs all the time.  We are not using the
 * global jiffies value, but a local "tics". I still want interrupt-less
 * jiffies in the real system, this is just a demo
 */
static volatile unsigned long tics;

#undef HZ
#define HZ  25 /* tics per second */

struct task {
	void (*job)(void);
	unsigned long release, period;
	unsigned long flags;
};
#define FLAG_RUNNING 1

void job_slow(void) /* run for 0.6 seconds, print dots */
{
	int i;

	putc('S');
	for (i = 0; i < 6; i++) {
		udelay(100 * 1000);
		putc('.');
	}
	putc('s');
}

void job_fast(void) /* run for 0.1s, only print enter/exit */
{
	putc('F');
	udelay(100 * 1000);
	putc('f');
}

struct task t_slow = {
	.job = job_slow,
	.release = 5, .period = HZ,
};

struct task t_fast = {
	.job = job_fast,
	.release = 3, .period = HZ * 3 / 10,
};

struct task *tasks[] = {
	&t_slow,
	&t_fast,
	NULL,
};

/* We keep track if the currently running task, if any */
struct task *current;

/* This is called both from main and at the timer interrupt */
void schedule(void *arg)
{
	struct task **p, *t, *best = NULL;
	struct task *prev;

	/* find best task, avoiding running ones */
	for (p = tasks; *p; p++) {
		t = *p;

		if (t->flags & FLAG_RUNNING)
			continue;
		if (current && current->period < t->period)
			continue;
		if (!best) {
			best = t;
			continue;
		}
		if (time_before(t->release, best->release))
			best = t;
	}
	if (!best)
		return;

	if (time_before(tics, best->release))
		return; /* wait for later tics */

	/* So run this this task, with housekeeping */
	prev = current;
	current = best;
	current->flags |= FLAG_RUNNING;
	current->job();
	current->release += current->period;
	current->flags &= ~FLAG_RUNNING;
	current = prev;
}


/*
 * Based on the above code, let's run a timer interrupt handler
 */
void __irq_entry(void)
{
	putc ('<');
	/* ack both the peripheral and the NVIC */
	regs[REG_TMR16B0IR] = 1; /* MR0 */
	regs[REG_NVIC_ICPR0] = lpc_irq_bit(LPC_IRQ_CT16B0);

	tics++;

	/* This is our assembly engine to call a user-space function */
	__sched(schedule, NULL);
	putc ('>');
}

void main(void)
{
	printf("Scheduler demo\n");
	unsigned long t = tics + HZ;

	/* Configure a periodic timer */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT16B0; /* power on */
	regs[REG_TMR16B0TCR] = 2; /* reset counters, disable */
	regs[REG_TMR16B0PR] = (CPU_FREQ / 1000) - 1; /* prescaler: 1kHz */
	regs[REG_TMR16B0MR0] = 40; /* match every 40ms */
	regs[REG_TMR16B0MCR] = 3; /* irq and reset counter */
	regs[REG_TMR16B0TCR] = 1; /* enable */

	/* Enable the associated interrupt */
	irq_enable();
	regs[REG_NVIC_ISER0] = lpc_irq_bit(LPC_IRQ_CT16B0);

	while (1) {
		/*
		 * deal with background jobs, possibly.
		 * Here we just print one newline every second
		 */
		while (time_before(tics, t))
			;
		putc('\n');
		t += HZ;
	}
}
