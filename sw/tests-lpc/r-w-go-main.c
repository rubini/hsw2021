#include <io.h>
#include <time.h>
#include <string.h>
#include <command.h>
#include <assert.h>
#include <errno.h>

/* Panic leds: yellow is clock, green is data */
struct panic_ledinfo panic_ledinfo = {
	.gpio_clock = GPIO_NR(0, 7),
	.gpio_data = GPIO_NR(1, 28),
};

static int command_go(char **reply, int argc, char **argv)
{
	int addr;
	char c;
	void (*f)(void);

	if (sscanf(argv[1], "%x%c", &addr, &c) != 1)
		return -EINVAL;
	f = (void *)addr;
	printf("going to %p\n", f);
	f();
	return 0;
}

/* assert <true-false> <ledvalue> */
static int command_assert(char **reply, int argc, char **argv)
{
	int cond, ledval;;
	char c;

	if (sscanf(argv[1], "%x%c", &cond, &c) != 1)
		return -EINVAL;
	if (sscanf(argv[2], "%x%c", &ledval, &c) != 1)
		return -EINVAL;
	assert(cond, ledval, "condition failed (ledval %s)\n", argv[2]);
	return 0;
}

/* sleep <seconds> [<milliseconds>] */
static int command_sleep(char **reply, int argc, char **argv)
{
	unsigned long j;
	int sec, msec = 0;
	char c;

	if (sscanf(argv[1], "%i%c", &sec, &c) != 1)
		return -EINVAL;
	if (argc == 3) {
		if (sscanf(argv[2], "%i%c", &msec, &c) != 1)
			return -EINVAL;
	}
	msec = sec * 1000 + msec;
	j = jiffies + (HZ * msec / 1000);
	while (time_before(jiffies, j))
		;
	return 0;
}

static struct command rwgo_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	COMMAND_HELP,
	{"go",    command_go,      2, 2},
	{"assert",command_assert,  3, 3},
	{"sleep", command_sleep,   2, 3},
	{}
};

void main(void)
{
	static char str[80];
	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);

	/* The three TDC leds */
	gpio_dir_af(GPIO_NR(1, 31), GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_NR(1, 28), GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_NR(0, 7), GPIO_DIR_OUT, 0, GPIO_AF_GPIO);


	while (1) {

		if (polls(str, sizeof(str))) {
			command_parse(str, rwgo_commands);
			puts(command_reply);
			str[0] = '\0';
		}
	}
}
