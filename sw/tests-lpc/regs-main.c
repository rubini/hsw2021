#include <io.h>
#include <time.h>
#include <debug-registers.h>

void main(void)
{
	unsigned long j = jiffies + 2 * HZ;
	int i;

	for (i = 0; ; i++) {
		registers_save();
		registers_print(&cpu_regs);
		while (time_before(jiffies, j))
		       ;
		j += 2 * HZ;
	}
}
