#include <io.h>
#include <time.h>
#include <task.h>

void task_serial(void *arg, struct task *self)
{
	char *sarg = arg;
	puts(sarg);
}

struct task __task t_quarter = {
	.name = "quarter",
	.job = task_serial,
	.arg = ".",
	.period = HZ / 4,
};
struct task __task t_second = {
	.name = "second",
	.job = task_serial,
	.arg = "S",
	.period = HZ,
	.release = 1,
};
struct task __task t_10s = {
	.name = "10s",
	.job = task_serial,
	.arg = "\n",
	.period = 10 * HZ,
	.release = 2,
};
struct task __task t_minute = {
	.name = "minute",
	.job = task_serial,
	.arg = "minute!\n",
	.period = 60 * HZ,
	.release = 3,
};

void main(void)
{
	scheduler(SCHED_VERBOSE_INIT);
}
