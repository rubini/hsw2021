PCBNEW-LibModule-V1  Wed May  9 09:55:46 2018
# encoding utf-8
Units mm
$INDEX
ADA-31850
LPC-H1xxx
N5110-nobrand
TIDE
arietta-mod
arietta-xterm
dc-dc_metric
dc-dc_module
enc28j60-h
motta
vocore2
$EndINDEX
$MODULE ADA-31850
Po 0 0 0 15 58DFC29C 00000000 ~~
Li ADA-31850
Sc 0
AR 
Op 0 0 0
T0 -5.08 11.43 1 1 0 0.15 N V 21 N "ADA-31850"
T1 7.62 11.43 1 1 0 0.15 N V 21 N "VAL**"
DS -10.16 -10.16 10.16 -10.16 0.15 21
DS 10.16 -10.16 10.16 10.16 0.15 21
DS 10.16 10.16 -10.16 10.16 0.15 21
DS -10.16 10.16 -10.16 -10.16 0.15 21
$PAD
Sh "1" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 7.62
$EndPAD
$PAD
Sh "2" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 7.62
$EndPAD
$PAD
Sh "3" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 7.62
$EndPAD
$PAD
Sh "4" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 7.62
$EndPAD
$PAD
Sh "5" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 7.62
$EndPAD
$PAD
Sh "6" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 7.62
$EndPAD
$PAD
Sh "7" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 7.62
$EndPAD
$PAD
Sh "8" C 1.6 1.6 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 7.62
$EndPAD
$PAD
Sh "" C 1.6 1.6 0 0 0
Dr 1.27 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -7.62 -7.62
$EndPAD
$PAD
Sh "" C 1.6 1.6 0 0 0
Dr 1.27 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 7.62 -7.62
$EndPAD
$EndMODULE ADA-31850
$MODULE LPC-H1xxx
Po 0 0 0 15 56D4B427 00000000 ~~
Li LPC-H1xxx
Sc 0
AR /56F4F154/57EA2B79
Op 0 0 0
T0 8.89 -12.7 1 1 0 0.15 N V 21 N "U18"
T1 10.16 -10.16 1 1 0 0.15 N V 21 N "LPC-H1XXX"
DS -19.05 -19.05 19.05 -19.05 0.15 21
DS 19.05 -19.05 19.05 19.05 0.15 21
DS 19.05 19.05 -19.05 19.05 0.15 21
DS -19.05 19.05 -19.05 -19.05 0.15 21
$PAD
Sh "1" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -13.97
$EndPAD
$PAD
Sh "2" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -11.43
$EndPAD
$PAD
Sh "3" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -8.89
$EndPAD
$PAD
Sh "4" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -6.35
$EndPAD
$PAD
Sh "6" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -3.81
$EndPAD
$PAD
Sh "7" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -1.27
$EndPAD
$PAD
Sh "9" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 1.27
$EndPAD
$PAD
Sh "10" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 3.81
$EndPAD
$PAD
Sh "11" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 6.35
$EndPAD
$PAD
Sh "12" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 8.89
$EndPAD
$PAD
Sh "13" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 11.43
$EndPAD
$PAD
Sh "14" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 13.97
$EndPAD
$PAD
Sh "15" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13.97 16.51
$EndPAD
$PAD
Sh "16" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.43 16.51
$EndPAD
$PAD
Sh "17" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 16.51
$EndPAD
$PAD
Sh "18" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 16.51
$EndPAD
$PAD
Sh "19" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 16.51
$EndPAD
$PAD
Sh "20" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 16.51
$EndPAD
$PAD
Sh "21" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 16.51
$EndPAD
$PAD
Sh "22" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 16.51
$EndPAD
$PAD
Sh "23" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 16.51
$EndPAD
$PAD
Sh "24" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 16.51
$EndPAD
$PAD
Sh "25" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.43 16.51
$EndPAD
$PAD
Sh "26" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.97 16.51
$EndPAD
$PAD
Sh "27" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 13.97
$EndPAD
$PAD
Sh "28" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 11.43
$EndPAD
$PAD
Sh "29" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 8.89
$EndPAD
$PAD
Sh "30" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 6.35
$EndPAD
$PAD
Sh "31" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 3.81
$EndPAD
$PAD
Sh "32" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 1.27
$EndPAD
$PAD
Sh "33" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -1.27
$EndPAD
$PAD
Sh "34" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -3.81
$EndPAD
$PAD
Sh "35" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -6.35
$EndPAD
$PAD
Sh "36" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -8.89
$EndPAD
$PAD
Sh "37" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -11.43
$EndPAD
$PAD
Sh "38" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -13.97
$EndPAD
$PAD
Sh "39" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.97 -16.51
$EndPAD
$PAD
Sh "40" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.43 -16.51
$EndPAD
$PAD
Sh "41" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 -16.51
$EndPAD
$PAD
Sh "42" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 -16.51
$EndPAD
$PAD
Sh "43" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 -16.51
$EndPAD
$PAD
Sh "44" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 -16.51
$EndPAD
$PAD
Sh "45" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 -16.51
$EndPAD
$PAD
Sh "46" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 -16.51
$EndPAD
$PAD
Sh "47" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 -16.51
$EndPAD
$PAD
Sh "48" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -16.51
$EndPAD
$PAD
Sh "PWR" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.43 -16.51
$EndPAD
$PAD
Sh "GND" C 1.7 1.7 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13.97 -16.51
$EndPAD
$EndMODULE LPC-H1xxx
$MODULE N5110-nobrand
Po 0 0 0 15 5A7F768F 00000000 ~~
Li N5110-nobrand
Sc 0
AR 
Op 0 0 0
T0 -11 16 1 1 0 0.15 N V 21 N "N5110-nobrand"
T1 16 20 1 1 0 0.15 N V 21 N "VAL**"
DS -22 22 22 22 0.15 21
DS 22 22 22 -22 0.15 21
DS 22 -22 -22 -22 0.15 21
DS -22 -22 -22 22 0.15 21
$PAD
Sh "8" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -19.685
$EndPAD
$PAD
Sh "7" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 -19.685
$EndPAD
$PAD
Sh "6" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 -19.685
$EndPAD
$PAD
Sh "5" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 -19.685
$EndPAD
$PAD
Sh "4" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 -19.685
$EndPAD
$PAD
Sh "3" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 -19.685
$EndPAD
$PAD
Sh "2" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 -19.685
$EndPAD
$PAD
Sh "1" R 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 -19.685
$EndPAD
$PAD
Sh "1" R 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 19.685
$EndPAD
$PAD
Sh "2" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 19.685
$EndPAD
$PAD
Sh "3" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 19.685
$EndPAD
$PAD
Sh "4" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 19.685
$EndPAD
$PAD
Sh "5" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 19.685
$EndPAD
$PAD
Sh "6" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 19.685
$EndPAD
$PAD
Sh "7" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 19.685
$EndPAD
$PAD
Sh "8" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 19.685
$EndPAD
$EndMODULE N5110-nobrand
$MODULE TIDE
Po 0 0 0 15 57E657DB 00000000 ~~
Li TIDE
Sc 0
AR /56F4F154/57EA2B88
Op 0 0 0
T0 -15.24 -12.7 1.524 1.524 0 0.3048 N V 21 N "P3"
T1 -13.97 -10.16 1.524 1.524 0 0.3048 N V 21 N "TIDE"
DS -19.05 -19.05 19.05 -19.05 0.05 21
DS 19.05 -19.05 19.05 19.05 0.05 21
DS 19.05 19.05 -19.05 19.05 0.05 21
DS -19.05 19.05 -19.05 -19.05 0.05 21
$PAD
Sh "1A" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 1.27
$EndPAD
$PAD
Sh "2A" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 -1.27
$EndPAD
$PAD
Sh "3A" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 1.27
$EndPAD
$PAD
Sh "4A" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -1.27
$EndPAD
$PAD
Sh "N1" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -16.51
$EndPAD
$PAD
Sh "N2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13.97 -16.51
$EndPAD
$PAD
Sh "N3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.43 -16.51
$EndPAD
$PAD
Sh "N4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -16.51
$EndPAD
$PAD
Sh "E1" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -16.51
$EndPAD
$PAD
Sh "E2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -13.97
$EndPAD
$PAD
Sh "E3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -11.43
$EndPAD
$PAD
Sh "E4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -8.89
$EndPAD
$PAD
Sh "S1" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 16.51
$EndPAD
$PAD
Sh "S2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.97 16.51
$EndPAD
$PAD
Sh "S3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.43 16.51
$EndPAD
$PAD
Sh "S4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 16.51
$EndPAD
$PAD
Sh "W1" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 16.51
$EndPAD
$PAD
Sh "W2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 13.97
$EndPAD
$PAD
Sh "W3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 11.43
$EndPAD
$PAD
Sh "W4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 8.89
$EndPAD
$PAD
Sh "2B" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 1.27
$EndPAD
$PAD
Sh "1B" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 -1.27
$EndPAD
$PAD
Sh "3B" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 -1.27
$EndPAD
$PAD
Sh "4B" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 1.27
$EndPAD
$EndMODULE TIDE
$MODULE arietta-mod
Po 0 0 0 15 586657EF 00000000 ~~
Li arietta-mod
Sc 0
AR 
Op 0 0 0
T0 0 28.5 1 1 0 0.15 N V 21 N "arietta-mod"
T1 0 0 1 1 0 0.15 N I 21 N "VAL**"
DC -10.7 24.7 -10.7 25.9 0.07874 21
DC -10.7 -24.7 -10.7 -23.5 0.07874 21
DS -12.5 26.5 -12.5 -26.5 0.07874 21
DS -12.5 -26.5 12.5 -26.5 0.07874 21
DS 12.5 -26.5 12.5 26.5 0.07874 21
DS 12.5 26.5 -12.5 26.5 0.07874 21
$PAD
Sh "1" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -24.13
$EndPAD
$PAD
Sh "2" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -24.13
$EndPAD
$PAD
Sh "3" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -21.59
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -21.59
$EndPAD
$PAD
Sh "5" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -19.05
$EndPAD
$PAD
Sh "6" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -19.05
$EndPAD
$PAD
Sh "7" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -16.51
$EndPAD
$PAD
Sh "8" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -16.51
$EndPAD
$PAD
Sh "9" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -13.97
$EndPAD
$PAD
Sh "10" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -13.97
$EndPAD
$PAD
Sh "11" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -11.43
$EndPAD
$PAD
Sh "12" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -11.43
$EndPAD
$PAD
Sh "13" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -8.89
$EndPAD
$PAD
Sh "14" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -8.89
$EndPAD
$PAD
Sh "15" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -6.35
$EndPAD
$PAD
Sh "16" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -6.35
$EndPAD
$PAD
Sh "17" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -3.81
$EndPAD
$PAD
Sh "18" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -3.81
$EndPAD
$PAD
Sh "19" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 -1.27
$EndPAD
$PAD
Sh "20" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 -1.27
$EndPAD
$PAD
Sh "21" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 1.27
$EndPAD
$PAD
Sh "22" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 1.27
$EndPAD
$PAD
Sh "23" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 3.81
$EndPAD
$PAD
Sh "24" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 3.81
$EndPAD
$PAD
Sh "25" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 6.35
$EndPAD
$PAD
Sh "26" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 6.35
$EndPAD
$PAD
Sh "27" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 8.89
$EndPAD
$PAD
Sh "28" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 8.89
$EndPAD
$PAD
Sh "29" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 11.43
$EndPAD
$PAD
Sh "30" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 11.43
$EndPAD
$PAD
Sh "31" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 13.97
$EndPAD
$PAD
Sh "32" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 13.97
$EndPAD
$PAD
Sh "33" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 16.51
$EndPAD
$PAD
Sh "34" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 16.51
$EndPAD
$PAD
Sh "35" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 19.05
$EndPAD
$PAD
Sh "36" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 19.05
$EndPAD
$PAD
Sh "37" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 21.59
$EndPAD
$PAD
Sh "38" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 21.59
$EndPAD
$PAD
Sh "39" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.56 24.13
$EndPAD
$PAD
Sh "40" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.1 24.13
$EndPAD
$PAD
Sh "41" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -19.82
$EndPAD
$PAD
Sh "42" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -17.28
$EndPAD
$PAD
Sh "43" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -14.74
$EndPAD
$PAD
Sh "44" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -12.2
$EndPAD
$PAD
Sh "45" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -9.66
$EndPAD
$PAD
Sh "46" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.1 -7.12
$EndPAD
$EndMODULE arietta-mod
$MODULE arietta-xterm
Po 0 0 0 15 5AF2A97A 00000000 ~~
Li arietta-xterm
Sc 0
AR 
Op 0 0 0
T0 -15 4 0.7 0.7 0 0.14 N V 21 N "arietta-xterm"
T1 0 3.81 1 1 0 0.15 N V 21 N "VAL**"
DC -23.495 -56.515 -24.765 -57.785 0.15 21
DC 41.275 -56.515 40.005 -57.785 0.15 21
DC 41.275 1.27 40.005 2.54 0.15 21
DS -27.305 -24.13 -28.575 -24.13 0.15 21
DS -28.575 -24.13 -28.575 -40.005 0.15 21
DS -28.575 -40.005 -11.43 -40.005 0.15 21
DS -11.43 -40.005 -11.43 -24.13 0.15 21
DS -11.43 -24.13 -27.305 -24.13 0.15 21
DS -27.305 5.08 43.815 5.08 0.15 21
DS -27.305 5.08 -27.305 -60.325 0.15 21
DS -27.305 -60.325 45.085 -60.325 0.15 21
DS 45.085 -60.325 45.085 -53.34 0.15 21
DS 43.815 5.08 45.085 5.08 0.15 21
DS 45.085 5.08 45.085 -3.175 0.15 21
DS 45.085 -3.81 45.085 -3.175 0.15 21
DS -24.765 -3.175 45.085 -3.175 0.15 21
DS -24.765 -3.175 -24.765 -53.34 0.15 21
DS -24.765 -53.34 45.085 -53.34 0.15 21
DS 45.085 -53.34 45.085 -3.81 0.15 21
DS -25.4 -2.54 25.4 -2.54 0.15 21
DS 25.4 -2.54 25.4 2.54 0.15 21
DS 25.4 2.54 -25.4 2.54 0.15 21
DS -25.4 2.54 -25.4 -2.54 0.15 21
$PAD
Sh "1" R 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -24.13 -1.27
$EndPAD
$PAD
Sh "2" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -24.13 1.27
$EndPAD
$PAD
Sh "3" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -21.59 -1.27
$EndPAD
$PAD
Sh "4" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -21.59 1.27
$EndPAD
$PAD
Sh "5" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -19.05 -1.27
$EndPAD
$PAD
Sh "6" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -19.05 1.27
$EndPAD
$PAD
Sh "7" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 -1.27
$EndPAD
$PAD
Sh "8" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -16.51 1.27
$EndPAD
$PAD
Sh "9" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13.97 -1.27
$EndPAD
$PAD
Sh "10" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13.97 1.27
$EndPAD
$PAD
Sh "11" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.43 -1.27
$EndPAD
$PAD
Sh "12" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11.43 1.27
$EndPAD
$PAD
Sh "13" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -1.27
$EndPAD
$PAD
Sh "14" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 1.27
$EndPAD
$PAD
Sh "15" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 -1.27
$EndPAD
$PAD
Sh "16" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6.35 1.27
$EndPAD
$PAD
Sh "17" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 -1.27
$EndPAD
$PAD
Sh "18" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 1.27
$EndPAD
$PAD
Sh "19" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 -1.27
$EndPAD
$PAD
Sh "20" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 1.27
$EndPAD
$PAD
Sh "21" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 -1.27
$EndPAD
$PAD
Sh "22" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 1.27
$EndPAD
$PAD
Sh "23" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 -1.27
$EndPAD
$PAD
Sh "24" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 1.27
$EndPAD
$PAD
Sh "25" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 -1.27
$EndPAD
$PAD
Sh "26" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.35 1.27
$EndPAD
$PAD
Sh "27" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 -1.27
$EndPAD
$PAD
Sh "28" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 1.27
$EndPAD
$PAD
Sh "29" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.43 -1.27
$EndPAD
$PAD
Sh "30" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.43 1.27
$EndPAD
$PAD
Sh "31" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.97 -1.27
$EndPAD
$PAD
Sh "32" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.97 1.27
$EndPAD
$PAD
Sh "33" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 -1.27
$EndPAD
$PAD
Sh "34" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.51 1.27
$EndPAD
$PAD
Sh "35" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 19.05 -1.27
$EndPAD
$PAD
Sh "36" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 19.05 1.27
$EndPAD
$PAD
Sh "37" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.59 -1.27
$EndPAD
$PAD
Sh "38" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.59 1.27
$EndPAD
$PAD
Sh "39" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 24.13 -1.27
$EndPAD
$PAD
Sh "40" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 24.13 1.27
$EndPAD
$EndMODULE arietta-xterm
$MODULE dc-dc_metric
Po 0 0 0 15 57EC2FFF 00000000 ~~
Li dc-dc_metric
Sc 0
AR /56F4F154/56F52351
Op 0 0 0
T0 10 23 1.524 1.524 0 0.3048 N V 21 N "P2"
T1 0 0 1 1 0 0.2 N I 21 N "DC-DC-metric"
DS 11 -21.5 11 21.5 0.2 21
DS -11 -21.5 11 -21.5 0.2 21
DS 11 21.5 -11 21.5 0.2 21
DS -11 21.5 -11 -21.5 0.2 21
$PAD
Sh "1" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9 -20
$EndPAD
$PAD
Sh "2" R 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -9 -20
$EndPAD
$PAD
Sh "3" R 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -9 20
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9 20
$EndPAD
$EndMODULE dc-dc_metric
$MODULE dc-dc_module
Po 0 0 0 15 55E952B5 00000000 ~~
Li dc-dc_module
Sc 0
AR /56F4F154/56F52351
Op 0 0 0
T0 -12.25 -6.25 1.524 1.524 0 0.3048 N V 21 N "P2"
T1 0 0 1.524 1.524 0 0.3048 N I 21 N "DC-DC_MODULE"
DS 10.16 -20.32 10.16 20.32 0.381 21
DS -10.16 -20.32 10.16 -20.32 0.381 21
DS 10.16 20.32 -10.16 20.32 0.381 21
DS -10.16 20.32 -10.16 -20.32 0.381 21
$PAD
Sh "1" C 1.524 1.524 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 -19.05
$EndPAD
$PAD
Sh "2" R 1.524 1.524 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 -19.05
$EndPAD
$PAD
Sh "3" R 1.524 1.524 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8.89 19.05
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.89 19.05
$EndPAD
$EndMODULE dc-dc_module
$MODULE enc28j60-h
Po 0 0 0 15 57EC3195 00000000 ~~
Li enc28j60-h
Sc 0
AR 
Op 0 0 0
T0 7.62 20.955 1 1 0 0.15 N V 21 N "enc28j60-h"
T1 -5.08 8.89 1 1 0 0.15 N V 21 N "VAL**"
DS -7.62 -13.97 -7.62 5.08 0.07874 21
DS -7.62 5.08 7.62 5.08 0.07874 21
DS 7.62 5.08 7.62 -13.97 0.07874 21
DS 7.62 -13.97 -7.62 -13.97 0.07874 21
DS -12.065 -10.16 12.065 -10.16 0.2 21
DS 12.065 -10.16 12.065 19.05 0.2 21
DS 12.065 19.05 -12.065 19.05 0.2 21
DS -12.065 19.05 -12.065 -10.16 0.2 21
$PAD
Sh "1" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10.16 -5.08
$EndPAD
$PAD
Sh "2" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10.16 -2.54
$EndPAD
$PAD
Sh "3" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10.16 0
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10.16 2.54
$EndPAD
$PAD
Sh "5" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10.16 5.08
$EndPAD
$PAD
Sh "6" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 5.08
$EndPAD
$PAD
Sh "7" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 2.54
$EndPAD
$PAD
Sh "8" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 0
$EndPAD
$PAD
Sh "9" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 -2.54
$EndPAD
$PAD
Sh "10" C 1.524 1.524 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 -5.08
$EndPAD
$EndMODULE enc28j60-h
$MODULE motta
Po 0 0 0 15 5A1B5A7E 00000000 ~~
Li motta
Sc 0
AR 
Op 0 0 0
T0 -21.59 -0.635 1 1 0 0.15 N V 21 N "motta"
T1 -23.495 -17.145 1 1 0 0.15 N V 21 N "VAL**"
DS -26.67 -19.05 43.18 -19.05 0.3 21
DS 43.18 -19.05 43.18 30.988 0.3 21
DS 43.18 30.988 -26.67 30.988 0.3 21
DS -26.67 30.988 -26.67 -19.05 0.3 21
DS -24.21 -15.43 -19.13 -15.43 0.07874 21
DS -19.13 -15.43 -19.13 -2.73 0.07874 21
DS -19.13 -2.73 -24.21 -2.73 0.07874 21
DS -24.21 -2.73 -24.21 -15.43 0.07874 21
DS 21.59 24.13 34.29 24.13 0.07874 21
DS 34.29 24.13 34.29 29.21 0.07874 21
DS 34.29 29.21 21.59 29.21 0.07874 21
DS 21.59 29.21 21.59 24.13 0.07874 21
DS 11.43 24.13 11.43 29.21 0.07874 21
DS 11.43 29.21 -1.27 29.21 0.07874 21
DS -1.27 29.21 -1.27 24.13 0.07874 21
DS -1.27 24.13 11.43 24.13 0.07874 21
DS 21.59 -1.27 34.29 -1.27 0.07874 21
DS 34.29 -1.27 34.29 3.81 0.07874 21
DS 34.29 3.81 21.59 3.81 0.07874 21
DS 21.59 3.81 21.59 -1.27 0.07874 21
DS -1.27 -1.27 11.43 -1.27 0.07874 21
DS 11.43 -1.27 11.43 3.81 0.07874 21
DS 11.43 3.81 -1.27 3.81 0.07874 21
DS -1.27 3.81 -1.27 -1.27 0.07874 21
$PAD
Sh "12" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "13" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 2.54
$EndPAD
$PAD
Sh "14" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 0
$EndPAD
$PAD
Sh "15" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 2.54
$EndPAD
$PAD
Sh "16" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 0
$EndPAD
$PAD
Sh "17" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.62 2.54
$EndPAD
$PAD
Sh "18" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.62 0
$EndPAD
$PAD
Sh "19" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 2.54
$EndPAD
$PAD
Sh "20" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 0
$EndPAD
$PAD
Sh "11" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 2.54
$EndPAD
$PAD
Sh "21" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 22.86 2.54
$EndPAD
$PAD
Sh "22" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 22.86 0
$EndPAD
$PAD
Sh "23" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 25.4 2.54
$EndPAD
$PAD
Sh "24" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 25.4 0
$EndPAD
$PAD
Sh "25" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 27.94 2.54
$EndPAD
$PAD
Sh "26" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 27.94 0
$EndPAD
$PAD
Sh "27" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 30.48 2.54
$EndPAD
$PAD
Sh "28" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 30.48 0
$EndPAD
$PAD
Sh "29" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 33.02 2.54
$EndPAD
$PAD
Sh "30" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 33.02 0
$EndPAD
$PAD
Sh "31" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 27.94
$EndPAD
$PAD
Sh "32" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 25.4
$EndPAD
$PAD
Sh "33" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 27.94
$EndPAD
$PAD
Sh "34" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 25.4
$EndPAD
$PAD
Sh "35" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 27.94
$EndPAD
$PAD
Sh "36" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 25.4
$EndPAD
$PAD
Sh "37" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.62 27.94
$EndPAD
$PAD
Sh "38" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.62 25.4
$EndPAD
$PAD
Sh "39" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 27.94
$EndPAD
$PAD
Sh "40" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.16 25.4
$EndPAD
$PAD
Sh "41" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 22.86 27.94
$EndPAD
$PAD
Sh "42" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 22.86 25.4
$EndPAD
$PAD
Sh "43" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 25.4 27.94
$EndPAD
$PAD
Sh "44" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 25.4 25.4
$EndPAD
$PAD
Sh "45" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 27.94 27.94
$EndPAD
$PAD
Sh "46" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 27.94 25.4
$EndPAD
$PAD
Sh "47" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 30.48 27.94
$EndPAD
$PAD
Sh "48" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 30.48 25.4
$EndPAD
$PAD
Sh "49" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 33.02 27.94
$EndPAD
$PAD
Sh "50" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 33.02 25.4
$EndPAD
$PAD
Sh "1" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -20.4 -4
$EndPAD
$PAD
Sh "2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -22.94 -4
$EndPAD
$PAD
Sh "3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -20.4 -6.54
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -22.94 -6.54
$EndPAD
$PAD
Sh "5" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -20.4 -9.08
$EndPAD
$PAD
Sh "6" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -22.94 -9.08
$EndPAD
$PAD
Sh "7" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -20.4 -11.62
$EndPAD
$PAD
Sh "8" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -22.94 -11.62
$EndPAD
$PAD
Sh "9" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -20.4 -14.16
$EndPAD
$PAD
Sh "10" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -22.94 -14.16
$EndPAD
$PAD
Sh "" C 3.4 3.4 0 0 0
Dr 3.2 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -6.65 25.95
$EndPAD
$PAD
Sh "" C 3.4 3.4 0 0 0
Dr 3.2 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 38.35 0.95
$EndPAD
$PAD
Sh "" C 3.4 3.4 0 0 0
Dr 3.2 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 38.35 25.95
$EndPAD
$EndMODULE motta
$MODULE vocore2
Po 0 0 0 15 5962BE31 00000000 ~~
Li vocore2
Sc 0
AR 
Op 0 0 0
T0 10.5156 14.5542 1 1 0 0.15 N V 21 N "vocore2"
T1 0 0 1 1 0 0.15 N I 21 N "VAL**"
DS 12.9032 -12.9032 12.9032 -11.8872 0.07874 21
DS 11.8872 -12.9032 12.9032 -12.9032 0.07874 21
DS 12.9032 12.9032 11.8872 12.9032 0.07874 21
DS 12.9032 11.8872 12.9032 12.9032 0.07874 21
DS -12.9032 11.8872 -12.9032 12.9032 0.07874 21
DS -12.9032 12.9032 -11.8872 12.9032 0.07874 21
DS -12.9032 -11.8872 -12.9032 -12.9032 0.07874 21
DS -12.9032 -12.9032 -11.8872 -12.9032 0.07874 21
$PAD
Sh "48" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 6.223 -12.9032
$EndPAD
$PAD
Sh "148" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 6.223 -12.9032
$EndPAD
$PAD
Sh "49" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 4.953 -12.9032
$EndPAD
$PAD
Sh "149" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 4.953 -12.9032
$EndPAD
$PAD
Sh "50" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 3.683 -12.9032
$EndPAD
$PAD
Sh "150" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 3.683 -12.9032
$EndPAD
$PAD
Sh "51" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 2.413 -12.9032
$EndPAD
$PAD
Sh "151" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 2.413 -12.9032
$EndPAD
$PAD
Sh "52" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 1.143 -12.9032
$EndPAD
$PAD
Sh "152" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 1.143 -12.9032
$EndPAD
$PAD
Sh "53" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -0.127 -12.9032
$EndPAD
$PAD
Sh "153" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -0.127 -12.9032
$EndPAD
$PAD
Sh "54" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -1.397 -12.9032
$EndPAD
$PAD
Sh "154" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -1.397 -12.9032
$EndPAD
$PAD
Sh "55" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -2.667 -12.9032
$EndPAD
$PAD
Sh "155" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -2.667 -12.9032
$EndPAD
$PAD
Sh "56" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -3.937 -12.9032
$EndPAD
$PAD
Sh "156" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -3.937 -12.9032
$EndPAD
$PAD
Sh "57" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -5.207 -12.9032
$EndPAD
$PAD
Sh "157" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -5.207 -12.9032
$EndPAD
$PAD
Sh "58" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -6.477 -12.9032
$EndPAD
$PAD
Sh "158" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -6.477 -12.9032
$EndPAD
$PAD
Sh "59" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -7.747 -12.9032
$EndPAD
$PAD
Sh "159" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -7.747 -12.9032
$EndPAD
$PAD
Sh "33" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 11.811
$EndPAD
$PAD
Sh "133" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 11.811
$EndPAD
$PAD
Sh "34" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 10.541
$EndPAD
$PAD
Sh "134" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 10.541
$EndPAD
$PAD
Sh "35" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 9.271
$EndPAD
$PAD
Sh "135" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 9.271
$EndPAD
$PAD
Sh "36" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 8.001
$EndPAD
$PAD
Sh "136" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 8.001
$EndPAD
$PAD
Sh "37" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 6.731
$EndPAD
$PAD
Sh "137" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 6.731
$EndPAD
$PAD
Sh "38" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 5.461
$EndPAD
$PAD
Sh "138" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 5.461
$EndPAD
$PAD
Sh "39" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 4.191
$EndPAD
$PAD
Sh "139" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 4.191
$EndPAD
$PAD
Sh "40" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 2.921
$EndPAD
$PAD
Sh "140" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 2.921
$EndPAD
$PAD
Sh "41" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 1.651
$EndPAD
$PAD
Sh "141" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 1.651
$EndPAD
$PAD
Sh "42" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 0.381
$EndPAD
$PAD
Sh "142" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 0.381
$EndPAD
$PAD
Sh "43" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 -0.889
$EndPAD
$PAD
Sh "143" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 -0.889
$EndPAD
$PAD
Sh "44" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 -2.159
$EndPAD
$PAD
Sh "144" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 -2.159
$EndPAD
$PAD
Sh "45" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 -3.429
$EndPAD
$PAD
Sh "145" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 -3.429
$EndPAD
$PAD
Sh "46" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 -4.699
$EndPAD
$PAD
Sh "146" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 -4.699
$EndPAD
$PAD
Sh "47" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 12.9032 -5.969
$EndPAD
$PAD
Sh "147" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 12.9032 -5.969
$EndPAD
$PAD
Sh "19" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -11.811 12.9032
$EndPAD
$PAD
Sh "119" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -11.811 12.9032
$EndPAD
$PAD
Sh "20" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -10.541 12.9032
$EndPAD
$PAD
Sh "120" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -10.541 12.9032
$EndPAD
$PAD
Sh "21" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -9.271 12.9032
$EndPAD
$PAD
Sh "121" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -9.271 12.9032
$EndPAD
$PAD
Sh "22" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -8.001 12.9032
$EndPAD
$PAD
Sh "122" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -8.001 12.9032
$EndPAD
$PAD
Sh "23" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -6.731 12.9032
$EndPAD
$PAD
Sh "123" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -6.731 12.9032
$EndPAD
$PAD
Sh "24" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -5.461 12.9032
$EndPAD
$PAD
Sh "124" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -5.461 12.9032
$EndPAD
$PAD
Sh "25" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -4.191 12.9032
$EndPAD
$PAD
Sh "125" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -4.191 12.9032
$EndPAD
$PAD
Sh "26" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -2.921 12.9032
$EndPAD
$PAD
Sh "126" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -2.921 12.9032
$EndPAD
$PAD
Sh "27" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -1.651 12.9032
$EndPAD
$PAD
Sh "127" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -1.651 12.9032
$EndPAD
$PAD
Sh "28" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -0.381 12.9032
$EndPAD
$PAD
Sh "128" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -0.381 12.9032
$EndPAD
$PAD
Sh "29" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 0.889 12.9032
$EndPAD
$PAD
Sh "129" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 0.889 12.9032
$EndPAD
$PAD
Sh "30" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 2.159 12.9032
$EndPAD
$PAD
Sh "130" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 2.159 12.9032
$EndPAD
$PAD
Sh "31" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 3.429 12.9032
$EndPAD
$PAD
Sh "131" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 3.429 12.9032
$EndPAD
$PAD
Sh "32" O 0.635 2.54 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po 4.699 12.9032
$EndPAD
$PAD
Sh "132" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po 4.699 12.9032
$EndPAD
$PAD
Sh "1" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -9.144
$EndPAD
$PAD
Sh "101" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -9.144
$EndPAD
$PAD
Sh "2" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -7.874
$EndPAD
$PAD
Sh "102" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -7.874
$EndPAD
$PAD
Sh "3" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -6.604
$EndPAD
$PAD
Sh "103" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -6.604
$EndPAD
$PAD
Sh "4" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -5.334
$EndPAD
$PAD
Sh "104" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -5.334
$EndPAD
$PAD
Sh "5" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -4.064
$EndPAD
$PAD
Sh "105" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -4.064
$EndPAD
$PAD
Sh "6" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -2.794
$EndPAD
$PAD
Sh "106" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -2.794
$EndPAD
$PAD
Sh "7" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -1.524
$EndPAD
$PAD
Sh "107" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -1.524
$EndPAD
$PAD
Sh "8" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 -0.254
$EndPAD
$PAD
Sh "108" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 -0.254
$EndPAD
$PAD
Sh "9" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 1.016
$EndPAD
$PAD
Sh "109" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 1.016
$EndPAD
$PAD
Sh "10" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 2.286
$EndPAD
$PAD
Sh "110" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 2.286
$EndPAD
$PAD
Sh "11" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 3.556
$EndPAD
$PAD
Sh "111" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 3.556
$EndPAD
$PAD
Sh "12" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 4.826
$EndPAD
$PAD
Sh "112" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 4.826
$EndPAD
$PAD
Sh "13" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 6.096
$EndPAD
$PAD
Sh "113" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 6.096
$EndPAD
$PAD
Sh "14" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 7.366
$EndPAD
$PAD
Sh "114" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 7.366
$EndPAD
$PAD
Sh "15" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 8.636
$EndPAD
$PAD
Sh "115" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 8.636
$EndPAD
$PAD
Sh "16" O 2.54 0.635 0 0 0
Dr 0 0 0
At SMD N 00808000
Ne 0 ""
Po -12.9032 9.906
$EndPAD
$PAD
Sh "116" R 1.27 0.635 0 0 0
Dr 0 0 0
At SMD N 00080000
Ne 0 ""
Po -12.9032 9.906
$EndPAD
$EndMODULE vocore2
$EndLIBRARY
