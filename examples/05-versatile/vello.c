#include "versatile.h"

static void puts(const char *s)
{
	while (*s)
		putc(*s++);
}

void vello(void)
{
	unsigned long j = jiffies + HZ;

	while (1) {
		puts("Hello, Versatile here\n");
		while (jiffies < j)
			;
		j += HZ;
	}
}
