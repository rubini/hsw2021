#include "hw.h"

extern volatile unsigned long _jiffies;

#define jiffies (~_jiffies)

extern int putc(int c);
