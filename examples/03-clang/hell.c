#include "putc.h"

extern void sys_exit(int);

void puts(const char *s)
{
	while(*s)
		putc(*s++);
}

void main(void)
{
	puts("Hello\n");
	sys_exit(0);
}
