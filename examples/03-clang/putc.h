extern int sys_write(int fd, const void *buf, int count);

#define putc(c) sys_write(/* stdout */ 1, &c, 1 /* count */)
