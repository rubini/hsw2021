.text

/* syscall number in r7, arguments in r0, r1, r2, r3, ... */

.globl sys_write
.type sys_write,%function
sys_write:
	ldr	r7, =4 /* __NR_write */
	swi	0x0
	mov	pc, lr
.size sys_write,.-sys_write

.globl sys_exit
.type sys_exit,%function
sys_exit:
	ldr	r7, =1 /* __NR_exit */
	swi	0x0
	mov	pc, lr
.size sys_exit,.-sys_exit
