#include "povacca.h"

int pv_add(struct pv_stack *s)
{
	s->vals[s->sp - 2] += s->vals[s->sp - 1];
	s->sp--;
	return 0;
}

pv_operand(add, "+", 2, pv_add);

int pv_sub(struct pv_stack *s)
{
	s->vals[s->sp - 2] -= s->vals[s->sp - 1];
	s->sp--;
	return 0;
}

pv_operand(sub, "-", 2, pv_sub);
