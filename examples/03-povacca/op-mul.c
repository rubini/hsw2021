#include "povacca.h"

int pv_mul(struct pv_stack *s)
{
	s->vals[s->sp - 2] *= s->vals[s->sp - 1];
	s->sp--;
	return 0;
}

pv_operand(mul1, "*", 2, pv_mul);
pv_operand(mul2, "x", 2, pv_mul);

int pv_div(struct pv_stack *s)
{
	if (s->vals[s->sp - 1] == 0)
		return -3;
	s->vals[s->sp - 2] /= s->vals[s->sp - 1];
	s->sp--;
	return 0;
}

pv_operand(div, "/", 2, pv_div);
