
/* This is made the old way; we want to use <linux/list.h> instead */

struct generic_list {
	struct generic_list *next;
	void *payload;
};

#define list_insert(h, new) \
	({(new)->next = (h); (h) = (new);})

#define list_extract(h) \
	({struct generic_list *res = (h); if (h) (h) = (h)->next; res;})

