#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linux_rbtree.h"

#define SLEN 16

/* The structure includes both the payload and the rbtree stuff. */
struct line_rb {
	char line[SLEN];
	struct rb_node rb;
};

/*
 * The following functions are derived from the example in <linux/rbtree.h>
 * The first of them, the search one, is not used as we have unique items.
 */

static void insert_line(struct line_rb *item,
			struct rb_root *root)
{
	struct rb_node **p = &root->rb_node;
	struct rb_node *parent = NULL;
	struct line_rb *lrb;

	while (*p)
	{
		parent = *p;
		lrb = rb_entry(parent, struct line_rb, rb);

		if (strcmp(item->line, lrb->line) < 0)
			p = &(*p)->rb_left;
		else
			p = &(*p)->rb_right;
	}

	rb_link_node(&item->rb, parent, p);
	rb_insert_color(&item->rb, root);
}

static void print_lines(struct rb_root *root)
{
	struct line_rb *lrb;
	struct rb_node *node;

	for (node = rb_first(root); node; node = rb_next(node)) {
		lrb = rb_entry(node, struct line_rb, rb);
		printf("%s", lrb->line);
	}
}

int main(int argc, char **argv)
{
	char line[SLEN];
	struct rb_root rbroot = {0,};
	struct line_rb *item;

	/* read data and insert in the rbtree */
	while (fgets(line, SLEN, stdin)) {
		item = malloc(sizeof(*item));
		memcpy(item->line, line, SLEN);
		insert_line(item, &rbroot);
	}

	/* write it back */
	print_lines(&rbroot);

	/* do not free, like we don't free the array */

	return 0;

}
