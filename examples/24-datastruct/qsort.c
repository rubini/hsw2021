#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Use qsort(3) over integers received on stdin */

#define SLEN 16
#define NSAMPLE 1024 /* initial count for array size */

/* define a strcmpt with proper arguments for qsort */
typedef int(*cmp_f)(const void *, const void *);
cmp_f qsort_strcmp = (cmp_f)strcmp;


int main(int argc, char **argv)
{
	char line[SLEN];
	int i = 0, j;
	int nsample = NSAMPLE;
	char (*samples)[SLEN]; /* This is realloced by powers of 2 */

	samples = malloc(SLEN * nsample);
	if (!samples)
		exit(1);

	/* read data */
	while (fgets(line, SLEN, stdin)) {
		memcpy(samples[i++], line, SLEN);
		if (i >= nsample) {
			nsample *= 2;
			samples = realloc(samples, SLEN * nsample);
			if (!samples)
				exit(2);
		}
	}

	/* sort */
	qsort(samples, i, sizeof(samples[0]), qsort_strcmp);

	/* write it back */
	for (j = 0; j < i; j++)
		printf("%s", samples[j]);

	/* do not free the array, like we don't free the rbtree */

	return 0;
}

