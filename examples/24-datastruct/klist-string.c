/* Alessandro Rubini 2010, public domain */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linux_list.h" /* copied from linux/list.h in 2.6.31 */

struct str_item {
	char str[16];
	struct list_head list;
};

/* This program reads integers from stdin and prints it reversed */
int main(int argc, char **argv)
{
	struct list_head head;
	struct list_head *list;
	struct str_item *item;
	char line[16];

	INIT_LIST_HEAD(&head);

	/* read data */
	while (fgets(line, 16, stdin)) {
		item = malloc(sizeof(*item));
		memcpy(item->str, line, sizeof(item->str));
		list_add(&item->list, &head);
	}

	/* write it back */
	list_for_each(list, &head) {
		item = container_of(list, struct str_item, list);
		printf("%s", item->str);
	}

	/* free it all */
	while (!list_empty(&head)) {
		list = head.next;
		item = container_of(list, struct str_item, list);
		list_del(list);
		free(item);
	}


	return 0;
}

